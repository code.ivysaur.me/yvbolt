SHELL:=/bin/bash
SOURCES=$(find . -name '*.go' -type f)
.DEFAULT_GOAL := dist

liblcl-2.2.3.zip:
	rm -f liblcl-2.2.3.zip
	wget 'https://github.com/ying32/govcl/releases/download/v2.2.3/liblcl-2.2.3.zip'
	
liblcl.so: liblcl-2.2.3.zip
	echo "154b4e4a1d5137a2ffe51cb4d0bf152dd997c12616ae30862775c0e4f0928e88 liblcl-2.2.3.zip" | sha256sum -c
	rm -f liblcl.so
	unzip -j liblcl-2.2.3.zip linux64-gtk2/liblcl.so -d .
	touch liblcl.so

liblcl.dll: liblcl-2.2.3.zip
	echo "154b4e4a1d5137a2ffe51cb4d0bf152dd997c12616ae30862775c0e4f0928e88 liblcl-2.2.3.zip" | sha256sum -c
	rm -f liblcl.dll
	unzip -j liblcl-2.2.3.zip win64/liblcl.dll -d .
	touch liblcl.dll

yvbolt: $(SOURCES)
	CGO_CFLAGS='-O2 -Wno-return-local-addr' GOOS=linux GOARCH=amd64 CGO_ENABLED=1 go build -trimpath -ldflags '-s -w'
	chmod 755 yvbolt

yvbolt.exe: $(SOURCES)
	CGO_CFLAGS='-O2 -Wno-return-local-addr' GOOS=windows GOARCH=amd64 CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc-win32 go build -trimpath -ldflags '-s -w -H windowsgui'
	upx --lzma yvbolt.exe

yvbolt.linux64.tar.xz: yvbolt liblcl.so
	rm -f yvbolt.linux64.tar.xz
	XZ_OPT='-T0 -9' tar caf yvbolt.linux64.tar.xz --owner=0 --group=0 yvbolt liblcl.so
	
yvbolt.win64.zip: yvbolt.exe liblcl.dll
	rm -f yvbolt.win64.zip
	zip -9 yvbolt.win64.zip yvbolt.exe liblcl.dll

.PHONY: dist
dist: yvbolt.linux64.tar.xz yvbolt.win64.zip

.PHONY: clean
clean:
	rm -f yvbolt.exe yvbolt yvbolt.linux64.tar.xz yvbolt.win64.zip

.PHONY: clean-all
clean-all: clean
	rm -f liblcl-2.2.3.zip liblcl.so liblcl.dll
