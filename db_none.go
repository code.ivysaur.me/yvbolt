package main

import (
	"github.com/ying32/govcl/vcl"
)

type noLoadedDatabase struct{}

func (n *noLoadedDatabase) DisplayName() string {
	return APPNAME
}

func (n *noLoadedDatabase) DriverName() string {
	return "No database selected"
}

func (n *noLoadedDatabase) RootElement() *vcl.TTreeNode {
	return nil
}

func (n *noLoadedDatabase) RenderForNav(f *TMainForm, ndata *navData) error {
	f.propertiesBox.SetText("Open a database to get started...")
	return nil
}

func (n *noLoadedDatabase) NavChildren(ndata *navData) ([]string, error) {
	return []string{}, nil
}

func (ld *noLoadedDatabase) NavContext(ndata *navData) ([]contextAction, error) {
	return nil, nil // No special actions are supported
}

func (n *noLoadedDatabase) Keepalive(ndata *navData) {
}

func (n *noLoadedDatabase) Close() {}
