package main

import (
	"fmt"
	"unicode/utf8"
)

func formatUtf8(in []byte) string {
	if !utf8.Valid(in) {
		return fmt.Sprintf("<<Invalid UTF-8 %q>>", in)
	}

	return string(in)
}

func formatAny(in interface{}) string {
	switch in := in.(type) {
	case []byte:
		return "<<binary>>"

	case string:
		return in

	default:
		return fmt.Sprintf("%#v", in)
	}
}
