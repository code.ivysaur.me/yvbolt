package debconf

import (
	"os"
	"testing"
)

func TestDebconfParse(t *testing.T) {
	src, err := os.Open(DefaultConfigDat)
	if err != nil {
		if os.IsNotExist(err) {
			t.Skip(err)
		}
		t.Fatal(err)
	}
	defer src.Close()

	db, err := Parse(src)
	if err != nil {
		t.Fatalf("Parse: %v", err)
	}

	if len(db.Entries) == 0 {
		t.Errorf("expected >0 entries, got %v", len(db.Entries))
	}

	if len(db.AllColumnNames) == 0 {
		t.Errorf("expected >0 column names, got %v", len(db.AllColumnNames))
	}
}
