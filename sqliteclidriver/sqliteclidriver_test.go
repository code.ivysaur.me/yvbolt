package sqliteclidriver

import (
	"database/sql"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSqliteCliDriver(t *testing.T) {
	db, err := sql.Open("sqliteclidriver", ":memory:")
	require.NoError(t, err)

	_, err = db.Exec(`CREATE TABLE my_test_table ( id INTEGER PRIMARY KEY, extra TEXT NOT NULL );`)
	require.NoError(t, err)

	_, err = db.Exec(`INSERT INTO my_test_table (id, extra) VALUES ( ? , ? ), ( ? , ? );`, 1337, "abcdef", 9001, "whoop")
	require.NoError(t, err)

	// Repeat this part to ensure we can make followup queries on the same connection
	for i := 0; i < 3; i++ {

		res, err := db.Query(`SELECT * FROM my_test_table ORDER BY id ASC;`)
		require.NoError(t, err)

		cols, err := res.Columns()
		require.NoError(t, err)
		require.EqualValues(t, cols, []string{"id", "extra"})

		var rowCount int = 0

		for res.Next() {
			rowCount++

			var idVal int
			var extraVal string
			err = res.Scan(&idVal, &extraVal)
			if err != nil {
				t.Fatal(err)
			}

			switch rowCount {
			case 1:
				require.EqualValues(t, 1337, idVal)
				require.EqualValues(t, "abcdef", extraVal)

			case 2:
				require.EqualValues(t, 9001, idVal)
				require.EqualValues(t, "whoop", extraVal)

			}
		}

		require.Equal(t, rowCount, 2)

	}
}

func TestSqliteCliDriverNoResults(t *testing.T) {
	db, err := sql.Open("sqliteclidriver", ":memory:")
	require.NoError(t, err)

	// Repeat this part to ensure we can make followup queries on the same connection
	for i := 0; i < 3; i++ {
		_, err = db.Query(`SELECT 1 AS expect_no_result WHERE 1=2`)
		require.NoError(t, err)

		// Mix of results and no-results
		rr := db.QueryRow(`SELECT 1 AS expect_result WHERE 1=1`)
		require.NoError(t, rr.Err())

		var result int64
		err = rr.Scan(&result)
		require.NoError(t, err)
		require.EqualValues(t, result, 1)
	}

}
