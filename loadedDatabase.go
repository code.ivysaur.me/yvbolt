package main

import (
	"errors"

	"github.com/ying32/govcl/vcl"
)

var ErrNavNotExist error = errors.New("The selected item no longer exists")

type contextAction struct {
	Name     string
	Callback func(sender vcl.IComponent, ndata *navData) error
}

// loadedDatabase is a DB-agnostic interface for each loaded database.
type loadedDatabase interface {
	DisplayName() string
	DriverName() string
	RootElement() *vcl.TTreeNode
	RenderForNav(f *TMainForm, ndata *navData) error
	NavChildren(ndata *navData) ([]string, error)
	NavContext(ndata *navData) ([]contextAction, error)
	Keepalive(ndata *navData)
	Close()
}

type queryableLoadedDatabase interface {
	ExecQuery(query string, resultArea *vcl.TStringGrid) error
}

type editableLoadedDatabase interface {
	ApplyChanges(f *TMainForm, ndata *navData) error
}

// navData is the .Data() pointer for each TTreeNode in the left-hand tree.
type navData struct {
	ld             loadedDatabase
	childrenLoaded bool
	bucketPath     []string
}
