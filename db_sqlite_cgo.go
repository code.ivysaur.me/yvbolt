//+build cgo

package main

import (
	"yvbolt/sqliteclidriver"

	sqlite3 "github.com/mattn/go-sqlite3"
)

func (ld *sqliteLoadedDatabase) DriverName() string {
	if _, ok := ld.db.Driver().(*sqliteclidriver.SCDriver); ok {
		return "SQLite (sqliteclidriver)"
	}

	ver1, _, _ := sqlite3.Version()
	return "SQLite " + ver1
}
