# yvbolt

A graphical interface for multiple databases using [GoVCL](https://z-kit.cc/en/).

## Features

- Native desktop application, running on Linux, Windows, and macOS
- Connect to multiple databases at once
- Browse table/bucket content
  - Use context menu to perform special table/bucket actions
- Run custom SQL queries
  - Select text to run partial query
- Safe handling for non-UTF8 key and data fields
- Supported databases:
  - Badger v4
  - Bolt
    - Recursive bucket support
    - Option to open as readonly for shared access
    - Supports editing
    - See also [qbolt](https://code.ivysaur.me/qbolt) for more/different functionality
  - Debconf
  - Pebble
  - Redis
  - SQLite
    - Drivers: mattn (CGo), modernc.org (no cgo), experimental command-line driver
    - Supports editing
    - Integrated vacuum and export commands

## License

The code in this project is licensed under the ISC license (see `LICENSE` file for details).

This project redistributes images from the famfamfam/silk icon set under the [CC-BY 2.5 license](http://creativecommons.org/licenses/by/2.5/).

This project includes trademarked logo images for each supported database type.

## Compiling

1. `CGO_ENABLED=1 go build`
2. [Download liblcl](https://github.com/ying32/govcl/releases/download/v2.2.3/liblcl-2.2.3.zip) for your platform, or [compile it yourself](https://github.com/ying32/liblcl) (tested with v2.2.3)
3. Place the liblcl library file in the same directory as `yvbolt`
4. Run `yvbolt` and use the main menu to open a database

## Changelog

2024-07-18 v0.7.0

- SQLite, Bolt: Initial support for editing data (insert, per-cell update, delete)
- SQLite: Add context menu actions for compact (vacuum), export, and drop table
- App: New grid widget
- App: Add refresh button
- App: Bigger window size, use icons for toolbars, better UI colours for Windows
- App: Prevent submitting blank queries to database
- Refactor database interface and error handling

[⬇️ Download for Windows x86_64](https://git.ivysaur.me/code.ivysaur.me/yvbolt/releases/download/v0.7.0/yvbolt.win64.zip)

[⬇️ Download for Linux x86_64](https://git.ivysaur.me/code.ivysaur.me/yvbolt/releases/download/v0.7.0/yvbolt.linux64.tar.xz)

2024-06-30 v0.6.0

- Debconf: Add as supported database
- SQLite: Support table names containing special characters
- SQLite: Improvements for experimental command-line driver 
- Redis: Improve connection dialog window position
- App: Cosmetic fixes for frame borders, help dialog, and Windows fonts+colours
- Build: Change compression parameters for release builds
- Build: Compile CGO with -O2 for release builds

[⬇️ Download for Windows x86_64](https://git.ivysaur.me/code.ivysaur.me/yvbolt/releases/download/v0.6.0/yvbolt.win64.zip)

[⬇️ Download for Linux x86_64](https://git.ivysaur.me/code.ivysaur.me/yvbolt/releases/download/v0.6.0/yvbolt.linux64.tar.xz)

2024-06-29 v0.5.0

- Pebble: Add as supported database
- Bolt: Support opening as readonly
- Bolt: Support creating new databases
- Bolt: Support adding/removing recursive child buckets
- SQLite: Support custom CLI driver that parses `/usr/bin/sqlite3 -json` output (experimental)
- Redis: Improve query parser to support quoted strings
- App: Support refreshing elements in nav tree
- App: Help menu option to show driver versions
- App: Add image icons for refresh and close context menu actions
- Build: Add makefile for cross-compiling release binaries

[⬇️ Download for Windows x86_64](https://git.ivysaur.me/code.ivysaur.me/yvbolt/releases/download/v0.5.0/yvbolt.win64.zip)

[⬇️ Download for Linux x86_64](https://git.ivysaur.me/code.ivysaur.me/yvbolt/releases/download/v0.5.0/yvbolt.linux64.tar.xz)

2024-06-23 v0.4.0

- Redis: Add as supported database
- Badger: Allow creating in-memory databases
- App: Allow selecting partial query text to execute
- App: Allow closing database connections from context menu
- App: Allow scrolling large content on Properties pane
- App: Preload recursive navigation
- App: Automatically switch to selected database when new connection is created
- App: Add help website link
- App: Add database logo images

2024-06-25 v0.3.0

- Badger: Add BadgerDB v4 as supported database
- SQLite: Add support for CGo-free SQLite driver under cross-compilation
- Bolt: Update Bolt to v1.4.0-alpha.1
- App: Add support for running custom queries
- App: Add status bar showing currently selected DB
- App: Fix missing icons in nav when selecting items 
- App: Fix extra quotemarks when browsing string content of database

2024-06-08 v0.2.0

- SQLite: Add SQLite support (now requires CGo)
- App: Add images for menu and navigation items

2024-06-03 v0.1.0

- Initial public release
