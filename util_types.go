package main

// box_interface creates a slice where all elements of the input slice are boxed
// in an interface{} type.
func box_interface[T any](input []T) []interface{} {
	ret := make([]interface{}, 0, len(input))
	for _, v := range input {
		ret = append(ret, v)
	}
	return ret
}

func int32slice_contains(haystack []int32, needle int32) bool {
	for _, v := range haystack {
		if v == needle {
			return true
		}
	}
	
	return false
}