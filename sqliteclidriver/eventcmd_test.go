package sqliteclidriver

import (
	"errors"
	"io"
	"os/exec"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEventCmd(t *testing.T) {
	cmd := exec.Command("/bin/bash", "-c", `echo "hello world"`)

	ch, _, err := ExecEvents(cmd)
	if err != nil {
		t.Fatal(err)
	}

	var consume []processEvent
	for ev := range ch {
		consume = append(consume, ev)
	}

	expect := []processEvent{
		processEvent{evtype: evtypeStdout, data: []byte("hello world\n")},
		processEvent{evtype: evtypeStdout, err: io.EOF},
		processEvent{evtype: evtypeStderr, err: io.EOF},
		processEvent{evtype: evtypeExit, err: nil},
	}

	require.EqualValues(t, expect, consume)
}

func TestEventCmdStdin(t *testing.T) {
	cmd := exec.Command("/usr/bin/tr", "a-z", "A-Z")

	ch, pw, err := ExecEvents(cmd)
	if err != nil {
		t.Fatal(err)
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	var consume []processEvent
	go func() {
		defer wg.Done()

		for ev := range ch {
			if ev.err != nil && errors.Is(ev.err, io.EOF) {
				continue // skip flakey ordering of two EOF statements
			}
			consume = append(consume, ev)
		}
	}()

	pw.Write([]byte("hello world"))
	pw.Close()

	wg.Wait()

	expect := []processEvent{
		processEvent{evtype: evtypeStdout, data: []byte("HELLO WORLD")},
		processEvent{evtype: evtypeExit, err: nil},
	}

	require.EqualValues(t, expect, consume)
}
