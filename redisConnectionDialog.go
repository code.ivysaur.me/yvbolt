package main

import (
	"fmt"

	"github.com/redis/go-redis/v9"
	"github.com/ying32/govcl/vcl"
	"github.com/ying32/govcl/vcl/types"
)

type TRedisConnectionDialogResult = *redis.Options

type TRedisConnectionDialog struct {
	*vcl.TForm

	Address         *vcl.TEdit
	Port            *vcl.TSpinEdit
	Password        *vcl.TEdit
	IsResp3Protocol *vcl.TCheckBox
	ConnectBtn      *vcl.TButton

	// CustomReturn must be set by the caller to a *redis.Options variable.
	CustomReturn *TRedisConnectionDialogResult // nil = no CustomReturn/cancelled dialog
}

func (f *TRedisConnectionDialog) OnFormCreate(sender vcl.IObject) {

	f.SetCaption("Connect to Redis...")
	f.SetWidth(320)
	f.SetHeight(160)
	f.SetPosition(types.PoOwnerFormCenter)

	// row 1

	row1 := vcl_row(f, 1)

	lblAddress := vcl.NewLabel(row1)
	lblAddress.SetParent(row1)
	lblAddress.SetCaption("Address:")
	lblAddress.SetAlign(types.AlLeft)
	lblAddress.SetLayout(types.TlCenter)
	lblAddress.SetLeft(1)
	lblAddress.BorderSpacing().SetAround(MY_SPACING)

	f.Address = vcl.NewEdit(row1)
	f.Address.SetParent(row1)
	f.Address.SetAlign(types.AlClient) // grow AlLeft)
	f.Address.SetWidth(MY_WIDTH)
	f.Address.SetLeft(2)
	f.Address.SetText("127.0.0.1")
	f.Address.BorderSpacing().SetAround(MY_SPACING)

	lblPort := vcl.NewLabel(row1)
	lblPort.SetParent(row1)
	lblPort.SetCaption(":")
	lblPort.SetAlign(types.AlRight)
	lblPort.SetLayout(types.TlCenter)
	lblPort.SetLeft(0)
	lblPort.SetAutoSize(true)
	lblPort.BorderSpacing().SetAround(MY_SPACING)

	f.Port = vcl.NewSpinEdit(row1)
	f.Port.SetParent(row1)
	f.Port.SetMinValue(1)
	f.Port.SetMaxValue(65535)
	f.Port.SetValue(6379) // Redis default port
	f.Port.SetAlign(types.AlRight)
	f.Port.SetLeft(1000)
	f.Port.BorderSpacing().SetAround(MY_SPACING)

	// row 2

	row2 := vcl_row(f, 2)

	lblPassword := vcl.NewLabel(row2)
	lblPassword.SetParent(row2)
	lblPassword.SetCaption("Password:")
	lblPassword.SetAlign(types.AlLeft)
	lblPassword.SetLayout(types.TlCenter)
	lblPassword.SetLeft(1)
	lblPassword.BorderSpacing().SetAround(MY_SPACING)

	f.Password = vcl.NewEdit(row2)
	f.Password.SetParent(row2)
	f.Password.SetAlign(types.AlClient) // grow
	f.Password.SetLeft(2)
	f.Password.SetWidth(MY_WIDTH)
	f.Password.SetPasswordChar(uint16('*'))
	f.Password.BorderSpacing().SetAround(MY_SPACING)

	// row 3

	f.IsResp3Protocol = vcl.NewCheckBox(f)
	f.IsResp3Protocol.SetParent(f)
	f.IsResp3Protocol.SetCaption("Use RESP v3 protocol")
	f.IsResp3Protocol.SetChecked(true)
	f.IsResp3Protocol.SetAlign(types.AlTop)
	f.IsResp3Protocol.SetTop(3)
	f.IsResp3Protocol.BorderSpacing().SetAround(MY_SPACING)

	// buttons

	row4 := vcl_row(f, 4)
	row4.SetAlign(types.AlBottom)

	f.ConnectBtn = vcl.NewButton(row4)
	f.ConnectBtn.SetParent(row4)
	f.ConnectBtn.SetAlign(types.AlRight)
	f.ConnectBtn.SetCaption("Connect")
	f.ConnectBtn.SetOnClick(f.OnConnectBtnClick)
}

func (f *TRedisConnectionDialog) OnConnectBtnClick(sender vcl.IObject) {

	ret := &redis.Options{
		Addr:     fmt.Sprintf("%s:%d", f.Address.Text(), f.Port.Value()),
		Password: f.Password.Text(),
		DB:       0, // Start off with default DB, but, we will browse them ourselves
		Protocol: 2, // RESP2
	}

	if f.IsResp3Protocol.Checked() {
		ret.Protocol = 3 // RESP3
	}

	// Success.
	// Fill in target pointer and close dialog with "MrOk" sentinel
	*f.CustomReturn = ret
	f.SetModalResult(types.MrOk)
}
