package main

import (
	"embed"

	"github.com/ying32/govcl/vcl"
)

//go:embed assets/*
var assetsFs embed.FS

const (
	imgAdd int32 = iota
	imgArrowRefresh
	imgChartBar
	imgDatabase
	imgDatabaseAdd
	imgDatabaseDelete
	imgDatabaseLightning
	imgDatabaseSave
	imgDelete
	imgLightning
	imgLightningGo
	imgPencil
	imgPencilAdd
	imgPencilDelete
	imgPencilGo
	imgResultsetNext
	imgTable
	imgTableAdd
	imgTableDelete
	imgTableSave
	imgVendorCockroach
	imgVendorDebian
	imgVendorDgraph
	imgVendorGithub
	imgVendorMySQL
	imgVendorRedis
	imgVendorSqlite
)

func loadImages(owner vcl.IComponent) *vcl.TImageList {

	mustLoad := func(n string) *vcl.TBitmap {
		imgData, err := assetsFs.ReadFile(n)
		if err != nil {
			panic(err)
		}

		png := vcl.NewPngImage()
		png.LoadFromBytes(imgData)

		ret := vcl.NewBitmap()
		ret.Assign(png)
		return ret
	}

	ilist := vcl.NewImageList(owner)

	// ls assets | sort | sed -re 's~(.+)~ilist.Add(mustLoad("assets/\1"), nil)~'
	ilist.Add(mustLoad("assets/add.png"), nil)
	ilist.Add(mustLoad("assets/arrow_refresh.png"), nil)
	ilist.Add(mustLoad("assets/chart_bar.png"), nil)
	ilist.Add(mustLoad("assets/database.png"), nil)
	ilist.Add(mustLoad("assets/database_add.png"), nil)
	ilist.Add(mustLoad("assets/database_delete.png"), nil)
	ilist.Add(mustLoad("assets/database_lightning.png"), nil)
	ilist.Add(mustLoad("assets/database_save.png"), nil)
	ilist.Add(mustLoad("assets/delete.png"), nil)
	ilist.Add(mustLoad("assets/lightning.png"), nil)
	ilist.Add(mustLoad("assets/lightning_go.png"), nil)
	ilist.Add(mustLoad("assets/pencil.png"), nil)
	ilist.Add(mustLoad("assets/pencil_add.png"), nil)
	ilist.Add(mustLoad("assets/pencil_delete.png"), nil)
	ilist.Add(mustLoad("assets/pencil_go.png"), nil)
	ilist.Add(mustLoad("assets/resultset_next.png"), nil)
	ilist.Add(mustLoad("assets/table.png"), nil)
	ilist.Add(mustLoad("assets/table_add.png"), nil)
	ilist.Add(mustLoad("assets/table_delete.png"), nil)
	ilist.Add(mustLoad("assets/table_save.png"), nil)
	ilist.Add(mustLoad("assets/vendor_cockroach.png"), nil)
	ilist.Add(mustLoad("assets/vendor_debian.png"), nil)
	ilist.Add(mustLoad("assets/vendor_dgraph.png"), nil)
	ilist.Add(mustLoad("assets/vendor_github.png"), nil)
	ilist.Add(mustLoad("assets/vendor_mysql.png"), nil)
	ilist.Add(mustLoad("assets/vendor_redis.png"), nil)
	ilist.Add(mustLoad("assets/vendor_sqlite.png"), nil)

	return ilist

}
