package main

import (
	"runtime/debug"
)

var (
	version_Pebble string
)

func init() {

	// ReadBuildInfo still works even with `-ldflags '-s -w'`.
	mm, ok := debug.ReadBuildInfo()
	if !ok {
		panic("Missing build info")
	}

	for _, dep := range mm.Deps {
		switch dep.Path {
		case "github.com/cockroachdb/pebble":
			version_Pebble = dep.Version
		}
	}
}
