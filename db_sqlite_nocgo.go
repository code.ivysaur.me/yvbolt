//+build !cgo

package main

import (
	"yvbolt/sqliteclidriver"

	_ "modernc.org/sqlite"
)

func (ld *sqliteLoadedDatabase) DriverName() string {
	if _, ok := ld.db.Driver().(*sqliteclidriver.SCDriver); ok {
		return "SQLite (sqliteclidriver)"
	}

	return "SQLite (modernc.org)"
}
