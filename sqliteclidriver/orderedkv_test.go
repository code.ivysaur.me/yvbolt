package sqliteclidriver

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestOrderedKV(t *testing.T) {

	input := `
{
	"zzz": "foo",
	"aaa": "bar"
}
`
	var got OrderedKV
	err := json.Unmarshal([]byte(input), &got)
	if err != nil {
		t.Fatal(err)
	}

	expect := OrderedKV{
		Pair{Key: "zzz", Value: "foo"},
		Pair{Key: "aaa", Value: "bar"},
	}

	require.EqualValues(t, expect, got)
}
