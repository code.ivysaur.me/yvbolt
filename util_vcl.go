package main

import (
	"runtime"

	"github.com/ying32/govcl/vcl"
	"github.com/ying32/govcl/vcl/types"
	"github.com/ying32/govcl/vcl/types/colors"
)

const (
	MY_SPACING = 6
	MY_HEIGHT  = 90
	MY_WIDTH   = 180

	MAX_AUTO_COL_WIDTH = 240
)

// vcl_row makes a TPanel row inside the target component.
func vcl_row(owner vcl.IWinControl, top int32) *vcl.TPanel {

	r := vcl.NewPanel(owner)
	r.SetParent(owner)
	r.SetBorderStyle(types.BsNone)
	r.SetBevelInner(types.BvNone)
	r.SetBevelOuter(types.BvNone)
	r.SetAlign(types.AlTop)
	r.SetTop(top)
	r.SetAutoSize(true)

	return r
}

// vcl_menuitem makes a child TMenuItem inside the parent menu.
func vcl_menuitem(parent *vcl.TMenuItem, caption string, imageIndex int32, onClick vcl.TNotifyEvent) *vcl.TMenuItem {
	m := vcl.NewMenuItem(parent)
	m.SetCaption(caption)
	if imageIndex != -1 {
		m.SetImageIndex(imageIndex)
	}
	if onClick != nil {
		m.SetOnClick(onClick)
	}
	parent.Add(m)

	return m
}

// vcl_menuseparator adds a separator to the parent TMenuItem.
func vcl_menuseparator(parent *vcl.TMenuItem) *vcl.TMenuItem {
	s := vcl.NewMenuItem(parent)
	s.SetCaption("-") // Creates separator
	parent.Add(s)

	return s
}

func vcl_default_tab_background() types.TColor {
	if runtime.GOOS == "windows" {
		// Assuming that uxtheme is loaded
		// @ref https://stackoverflow.com/a/20332712
		// return colors.ClBtnHighlight

		// None of the colors.** constants seem to be quite right on a test
		// Windows 11 machine - should be #f9f9f9
		return 0x00f9f9f9
	} else {
		return colors.ClBtnFace // 0x00f0f0f0
	}
}

func vcl_stringgrid_clear(d *vcl.TStringGrid) {
	d.SetFixedCols(0) // No left-hand cols
	d.SetRowCount(1)  // The 0th row is the column headers
	d.SetEnabled(false)
	d.Columns().Clear()
	d.Invalidate()
}

func vcl_stringgrid_columnwidths(d *vcl.TStringGrid) {
	// Skip slow processing for very large result sets
	if d.RowCount() > 1000 {
		return
	}

	d.AutoAdjustColumns()

	// AutoAdjustColumns will leave some columns massively too large by themselves
	// Reign them back in
	ct := d.Columns().Count()
	for i := int32(0); i < ct; i++ {
		if d.ColWidths(i) > MAX_AUTO_COL_WIDTH {
			d.SetColWidths(i, MAX_AUTO_COL_WIDTH)
		}
	}
}

func vcl_confirm_dialog(sender vcl.IComponent, title string, message string) bool {

	dlg := vcl.NewTaskDialog(sender)
	dlg.SetCaption(APPNAME)
	dlg.SetTitle(title)
	dlg.SetText(message)
	dlg.SetCommonButtons(types.NewSet())

	yesBtn := dlg.Buttons().Add()
	yesBtn.SetCaption("Confirm")
	yesBtn.SetModalResult(types.MrYes)

	noBtn := dlg.Buttons().Add()
	noBtn.SetCaption("Cancel")
	noBtn.SetModalResult(types.MrCancel)

	ret := dlg.Execute()
	if !ret {
		return false // dialog closed
	}
	if dlg.ModalResult() != types.MrYes {
		return false // other button clicked
	}

	return true // confirmed
}
